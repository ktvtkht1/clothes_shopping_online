package com.example.paymentservice.dto;

import com.example.paymentservice.entity.PaymentMethod;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class PaymentResponse implements Serializable {
    private Long paymentId;
    private Long orderId;
    private String status;
    private PaymentMethod paymentMethod;
    private int quantity;
    private float totalAmount;
    private Date paymentDate;


}
