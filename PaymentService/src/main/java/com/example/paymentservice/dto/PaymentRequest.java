package com.example.paymentservice.dto;

import com.example.paymentservice.entity.PaymentMethod;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;
@Data
@Builder
@AllArgsConstructor
public class PaymentRequest implements Serializable {
    private Long orderId;
    private PaymentMethod paymentMethod;
    private int quantity;
    private float totalAmount;

}
