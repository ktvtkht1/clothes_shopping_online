package com.example.paymentservice.repository;

import com.example.paymentservice.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
        public Payment findByOrderId(Long orderId);

       // public Payment createPayment(Payment payment);
                //   return paymentRepository.save(payment);


        //public Payment getPayment(Long id);
        // return paymentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Payment not found"));

}
