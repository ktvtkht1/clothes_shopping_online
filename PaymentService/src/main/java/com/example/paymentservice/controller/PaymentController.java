//package com.example.paymentservice.controller;
//
//import com.example.paymentservice.dto.PaymentRequest;
//import com.example.paymentservice.dto.PaymentResponse;
//import com.example.paymentservice.entity.Payment;
//import com.example.paymentservice.entity.PaymentMethod;
//import com.example.paymentservice.repository.PaymentRepository;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cache.annotation.Cacheable;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.client.RestTemplate;
//import java.util.*;
//
//@RestController
//@RequestMapping("/api/payment")
//public class PaymentController {
//    @Autowired
//    PaymentRepository paymentRepository;
//    @Autowired
//    RestTemplate restTemplate;
//    //lay tat ca danh sach hoa don da thanh toan
//    @GetMapping("/all")
//    //@Cacheable(value = "findAllPayment", key = "'findAllPayment'")
//    public List<Payment> findAllPayment() {
//        List<Payment> list = new ArrayList<>();
//        list = paymentRepository.findAll();
//        return list;
//    }
//    // post 1 thanh toan hoa don
//    @PostMapping("/pay")
//    public ResponseEntity<String> thanhToan(@RequestBody PaymentRequest paymentRequest) {
//        Payment payment = new Payment();
//        payment.setOrderId(paymentRequest.getOrderId());
//        payment.setPaymentDate(new Date());
//        payment.setStatus("SUCCESS");
//        payment.setPaymentMethod(paymentRequest.getPaymentMethod().name());
//        payment.setReferenceNumber(UUID.randomUUID().toString());
//        payment.setTotalAmount(paymentRequest.getTotalAmount());
//        payment.setQuantity(paymentRequest.getQuantity());
//        paymentRepository.save(payment);
//        return new ResponseEntity<String>("Thanh toán thành công hoá đơn\t" + paymentRequest.getOrderId(),
//                HttpStatus.OK);
//    }
//
//    @GetMapping("/pay/{id}")
//    //@Cacheable(value = "findByIdPayment", key = "'findByIdPayment'")
//    public ResponseEntity<PaymentResponse> findByIdPayment(@PathVariable long id) {
//        Optional<Payment> optional = paymentRepository.findById(id);
//        Payment payment = null;
//        if (optional.isPresent()) {
//            payment = optional.get();
//        } else {
//            throw new RuntimeException("Không có id theo " + id);
//        }
//        PaymentResponse paymentReponse = new PaymentResponse();
//        paymentReponse.setPaymentId(payment.getId());
//        paymentReponse.setOrderId(payment.getOrderId());
//        paymentReponse.setTotalAmount(payment.getTotalAmount());
//        paymentReponse.setPaymentDate(new Date());
//        paymentReponse.setPaymentMethod(PaymentMethod.valueOf(payment.getPaymentMethod()));
//        paymentReponse.setStatus(payment.getStatus());
//        paymentReponse.setQuantity(payment.getQuantity());
//        return new ResponseEntity<PaymentResponse>(paymentReponse, HttpStatus.OK);
//    }
//
//    @GetMapping("/pay/{orderId}")
//    @Cacheable(value = "findByOrderId", key = "'findByOrderId'")
//    public ResponseEntity<PaymentResponse> findByOrderId(@PathVariable long orderId) {
//        Payment payment = paymentRepository.findByOrderId(orderId);
//        PaymentResponse paymentReponse = new PaymentResponse();
//        paymentReponse.setPaymentId(payment.getId());
//        paymentReponse.setOrderId(payment.getOrderId());
//        paymentReponse.setTotalAmount(payment.getTotalAmount());
//        paymentReponse.setPaymentDate(new Date());
//        paymentReponse.setPaymentMethod(PaymentMethod.valueOf(payment.getPaymentMethod()));
//        paymentReponse.setStatus(payment.getStatus());
//        paymentReponse.setQuantity(payment.getQuantity());
//        return new ResponseEntity<PaymentResponse>(paymentReponse, HttpStatus.OK);
//    }
//}
package com.example.paymentservice.controller;

import com.example.paymentservice.dto.PaymentRequest;
import com.example.paymentservice.dto.PaymentResponse;
import com.example.paymentservice.entity.Payment;
import com.example.paymentservice.entity.PaymentMethod;
import com.example.paymentservice.repository.PaymentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {
    @Autowired
    PaymentRepository paymentRepository;

    // Lấy tất cả danh sách hóa đơn đã thanh toán
    @GetMapping("/all")
    @Cacheable(value = "payments")
    public List<Payment> findAllPayment() {
        return paymentRepository.findAll();
    }

    // Post một thanh toán hóa đơn
    @PostMapping("/pay")
    public ResponseEntity<String> thanhToan(@RequestBody PaymentRequest paymentRequest) {
        Payment payment = new Payment();
        payment.setOrderId(paymentRequest.getOrderId());
        payment.setPaymentDate(new Date());
        payment.setStatus("SUCCESS");
        payment.setPaymentMethod(paymentRequest.getPaymentMethod().name());
        payment.setReferenceNumber(UUID.randomUUID().toString());
        payment.setTotalAmount(paymentRequest.getTotalAmount());
        payment.setQuantity(paymentRequest.getQuantity());
        paymentRepository.save(payment);
        return new ResponseEntity<>("Thanh toán thành công hóa đơn " + paymentRequest.getOrderId(), HttpStatus.OK);
    }

    @GetMapping("/pay/{id}")
    @Cacheable(value = "payment", key = "#id")
    public ResponseEntity<PaymentResponse> findByIdPayment(@PathVariable long id) {
        Optional<Payment> optional = paymentRepository.findById(id);
        if (optional.isPresent()) {
            Payment payment = optional.get();
            PaymentResponse paymentResponse = new PaymentResponse();
            paymentResponse.setPaymentId(payment.getId());
            paymentResponse.setOrderId(payment.getOrderId());
            paymentResponse.setTotalAmount(payment.getTotalAmount());
            paymentResponse.setPaymentDate(payment.getPaymentDate());
            paymentResponse.setPaymentMethod(PaymentMethod.valueOf(payment.getPaymentMethod()));
            paymentResponse.setStatus(payment.getStatus());
            paymentResponse.setQuantity(payment.getQuantity());
            return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/pay/order/{orderId}")
    @Cacheable(value = "paymentByOrderId", key = "#orderId")
    public ResponseEntity<PaymentResponse> findByOrderId(@PathVariable long orderId) {
        Payment payment = paymentRepository.findByOrderId(orderId);
        if (payment != null) {
            PaymentResponse paymentResponse = new PaymentResponse();
            paymentResponse.setPaymentId(payment.getId());
            paymentResponse.setOrderId(payment.getOrderId());
            paymentResponse.setTotalAmount(payment.getTotalAmount());
            paymentResponse.setPaymentDate(payment.getPaymentDate());
            paymentResponse.setPaymentMethod(PaymentMethod.valueOf(payment.getPaymentMethod()));
            paymentResponse.setStatus(payment.getStatus());
            paymentResponse.setQuantity(payment.getQuantity());
            return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // PUT phương thức để sửa thông tin thanh toán
    @PutMapping("/pay/{id}")
    @CachePut(value = "payment", key = "#id")
    public ResponseEntity<String> updatePayment(@PathVariable long id, @RequestBody PaymentRequest paymentRequest) {
        Optional<Payment> optional = paymentRepository.findById(id);
        if (optional.isPresent()) {
            Payment payment = optional.get();
            payment.setOrderId(paymentRequest.getOrderId());
            payment.setTotalAmount(paymentRequest.getTotalAmount());
            payment.setQuantity(paymentRequest.getQuantity());
            payment.setPaymentMethod(paymentRequest.getPaymentMethod().name());
            paymentRepository.save(payment);
            return new ResponseEntity<>("Cập nhật thành công hóa đơn " + paymentRequest.getOrderId(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Không tìm thấy hóa đơn với id " + id, HttpStatus.NOT_FOUND);
        }
    }

    // DELETE phương thức để xóa thông tin thanh toán
    @DeleteMapping("/pay/{id}")
    @CacheEvict(value = "payment", key = "#id")
    public ResponseEntity<String> deletePayment(@PathVariable long id) {
        Optional<Payment> optional = paymentRepository.findById(id);
        if (optional.isPresent()) {
            paymentRepository.deleteById(id);
            return new ResponseEntity<>("Xóa thành công hóa đơn với id " + id, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Không tìm thấy hóa đơn với id " + id, HttpStatus.NOT_FOUND);
        }
    }
}

