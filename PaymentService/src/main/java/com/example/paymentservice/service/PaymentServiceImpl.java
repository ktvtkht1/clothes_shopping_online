//package com.example.paymentservice.service;
//
//import com.example.paymentservice.dto.PaymentRequest;
//import com.example.paymentservice.dto.PaymentResponse;
//import com.example.paymentservice.entity.Payment;
//import com.example.paymentservice.entity.PaymentMethod;
//import com.example.paymentservice.repository.PaymentRepository;
//import lombok.extern.log4j.Log4j2;
//import org.springframework.stereotype.Service;
//
//import java.util.Date;
//
//@Service
//@Log4j2
//public class PaymentServiceImpl implements PaymentService{
//    private final PaymentRepository paymentRepository;
//
//    public PaymentServiceImpl(PaymentRepository paymentRepository) {
//        this.paymentRepository = paymentRepository;
//    }
//    @Override
//    public Long doPayment(PaymentRequest makePayment) {
//        log.info("Recording payment details{}", makePayment);
//
//        Payment payment = Payment.builder()
//                .paymentDate(new Date()) //date hien tai
//                .paymentMethod(makePayment.getPaymentMethod().name())
//                .status("SUCCESS")
//                .orderId(makePayment.getOrderId())
//                .referenceNumber(makePayment.getReferenceNumber())
//                .quantity(makePayment.getQuantity())
//                .totalAmount(makePayment.getTotalAmount())
//                .build();
//        log.info("Saving transaction...");
//        paymentRepository.save(payment);
//
//        log.info("Transaction completed with id: {}", payment.getId());
//
//        return payment.getId();
//    }
//
//    @Override
//    public PaymentResponse getPaymentDetailsByOrderId(Long orderId) {
//        log.info("Getting payment details for the order id: {}", orderId);
//
//        Payment payment = paymentRepository.findByOrderId(orderId);
//
//        return PaymentResponse.builder()
//                .paymentId(payment.getId())
//                .paymentMethod(PaymentMethod.valueOf(payment.getPaymentMethod()))
//                .paymentDate(payment.getPaymentDate())
//                .OrderId(payment.getOrderId())
//                .status(payment.getStatus())
//                .build();
//
//    }
//}
