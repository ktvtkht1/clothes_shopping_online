package com.example.orderservice.service;


import com.example.orderservice.entity.Order;
import com.example.orderservice.entity.OrderItem;
import com.example.orderservice.exception.ServiceException;
import com.example.orderservice.repository.OrderRepository;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Log4j2
public class OrderService {
    private final OrderRepository orderRepository;


    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;


    }


    @Transactional // Ensure this method runs within a transactional context
    public List<Order> getAllOrder() {
        // Fetch orders along with their order items eagerly
        List<Order> orders = orderRepository.findAllWithOrderItems();
        return orders;
    }

    public Order addOrder(Order order) {
        Long totalPrice = calculateTotalPrice(order.getOrderItems());
        order.setTotalPrice(totalPrice);
        order.setOrderDate(LocalDateTime.now());

        // Lưu đối tượng order vào cơ sở dữ liệu và trả về
        return orderRepository.save(order);
    }

    private Long calculateTotalPrice(List<OrderItem> orderItems) {
        Long totalPrice = 0L;
        for (OrderItem orderItem : orderItems) {
            totalPrice += orderItem.getPrice() * orderItem.getQuantity();
        }
        return totalPrice;
    }

    public Order updateOrder(Order order) {
        // Check if the order object has a non-null ID
        if (order.getId() == null) {
            log.error("Order ID is null");
            throw new ServiceException("Order ID is null");
        }

        // Retrieve the existing order from the database
        Order existingOrder = orderRepository.findById(order.getId())
                .orElseThrow(() -> {
                    log.error("Order not found with ID: {}", order.getId());
                    return new ServiceException("Order not found with ID: " + order.getId());
                });

        // Update the existing order with the new data
        existingOrder.setUserId(order.getUserId());
        existingOrder.setOrderItems(order.getOrderItems());
        existingOrder.setTotalPrice(calculateTotalPrice(order.getOrderItems()));
        existingOrder.setStatus(order.getStatus());
        existingOrder.setPaymentMethod(order.getPaymentMethod());
        existingOrder.setDeliveryAddress(order.getDeliveryAddress());
        existingOrder.setOrderDate(order.getOrderDate());
        existingOrder.setShippingDate(order.getShippingDate());
        existingOrder.setDeliveryDate(order.getDeliveryDate());

        // Save and return the updated order
        return orderRepository.save(existingOrder);
    }


    public Order getOrder(Long id) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            log.error("Order not found");
            throw new ServiceException("Order not found with id : " + id);
        }
        return order;
    }

    public void deleteOrder(Long id) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            log.error("Order not found");
            throw new ServiceException("Order not found with id : " + id);
        }
        orderRepository.delete(order);
    }
}
