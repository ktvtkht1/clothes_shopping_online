package com.example.orderservice.entity;

public enum PaymentMethod {
    CREDIT_CARD,
    DEBIT_CARD,
    PAYPAL,
    CASH_ON_DELIVERY,
    BANK_TRANSFER,
    E_WALLET
}
