package com.example.orderservice.controller;

//import org.springframework.security.access.prepost.PreAuthorize;

import com.example.orderservice.entity.Order;
import com.example.orderservice.request.AddOrderRequest;
import com.example.orderservice.response.WrapResponse;
import com.example.orderservice.service.OrderService;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/order")
@Log4j2
public class OrderController {
    @Autowired
    private OrderService orderService;
//
//
//
    @GetMapping("/all")
    public WrapResponse<List<Order>> getAllOrder() {
        log.info("Get all Order");
        return WrapResponse.ok(orderService.getAllOrder());
    }
//
    @PostMapping("/add")
    public WrapResponse<Order> addOrder(@RequestBody @Valid Order order) {
        log.info("Add Order {} ", order);
        return WrapResponse.ok(orderService.addOrder(order));
    }

    @PostMapping("/update")
    public WrapResponse<Order> updateClothes(@RequestBody @Valid Order order) {
        log.info("Update order {} ", order);
        return WrapResponse.ok(orderService.updateOrder(order));
    }

    @GetMapping("/get/{id}")
    public WrapResponse<Order> getOrder(@PathVariable Long id) {
        log.info("Get Order by id {} ", id);
        return WrapResponse.ok(orderService.getOrder(id));
    }


    @PostMapping("/delete/{id}")
    public WrapResponse<String> deleteOrder(@PathVariable Long id) {
        log.info("Delete Order by id {} ", id);
        orderService.deleteOrder(id);
        return WrapResponse.ok("Delete Order success");
    }

}
