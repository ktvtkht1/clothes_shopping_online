package com.iuh.serviceuser.dto;


import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class OrderItemDto {
    @NotNull
    private Long clothesId;

    @NotNull
    private Integer quantity;

    @NotNull
    private Long price;
}