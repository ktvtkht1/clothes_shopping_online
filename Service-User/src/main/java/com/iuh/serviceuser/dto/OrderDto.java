package com.iuh.serviceuser.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderDto {
    @NotNull
    private Long userId;

    @NotNull
    private List<OrderItemDto> orderItems;

    @NotNull
    private Long totalPrice;

    @NotNull
    private String status;

    @NotNull
    private String paymentMethod;

    @NotNull
    private String deliveryAddress;

    @NotNull
    private LocalDateTime orderDate;

    private LocalDateTime shippingDate;

    private LocalDateTime deliveryDate;
}
