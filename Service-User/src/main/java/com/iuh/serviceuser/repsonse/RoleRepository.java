package com.iuh.serviceuser.repsonse;

import com.iuh.serviceuser.entity.ERole;
import com.iuh.serviceuser.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(ERole name);
}
