package com.iuh.serviceuser.controller;

import com.iuh.serviceuser.dto.OrderDto;
import com.iuh.serviceuser.entity.Cart;
import com.iuh.serviceuser.entity.CartItem;
import com.iuh.serviceuser.entity.Clothes;
import com.iuh.serviceuser.entity.Order;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import jakarta.validation.Valid;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth/test")
public class TestController {
    private static  final String BASE_URL = "http://localhost:8080/";
    private static  final String UserService = "UserService";

    private final RestTemplate restTemplate;

    public TestController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/all")
    public String allAccess(){
        return "Public Content !";
    }


    @GetMapping("/user")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public String userAccess(){
        return "User Content.";
    }

    @GetMapping("/mod")
    @PreAuthorize("hasRole('MODERATOR')")
    public String moderatorAccess(){
        return "Moderator Board.";
    }


    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminAccess(){
        return "Admin Board.";
    }


    // Test get all clothes from ClothesService with role ADMIN
    @GetMapping("/admin/clothes")
    @PreAuthorize("hasRole('ADMIN')")
//    @CircuitBreaker(name = UserService, fallbackMethod = "fallbackClothes")
//    @Retry(name = UserService)
    @RateLimiter(name = UserService, fallbackMethod = "fallbackRateLimiter")
    public ResponseEntity<Object> adminGetAllClothes() {
        int count = 1;
        System.out.println("Retry method called "+ count++ + " times at "+ new Date());
        ResponseEntity<Object> responseEntity = restTemplate.getForEntity(BASE_URL + "api/clothes/all", Object.class);
        return ResponseEntity.status(responseEntity.getStatusCode())
                .body(responseEntity.getBody());
    }

    //fallback method
    public ResponseEntity<Object> fallbackClothes(Exception e) {
        return ResponseEntity.status(500).body("Clothes service is down");
    }
    public ResponseEntity<Object> fallbackRateLimiter(Exception e) {
        return ResponseEntity.status(500).body("Bạn không nên truy cập quá nhanh");
    }
    public ResponseEntity<Object> fallbackCart(Exception e) {
        return ResponseEntity.status(500).body("Cart service is down");
    }
    public ResponseEntity<Object> fallbackOrder(Exception e) {
        return ResponseEntity.status(500).body("Order service is down");
    }



    @PostMapping("/user/cart/add")
//    @PreAuthorize("hasRole('USER')")
    @Retry(name = UserService, fallbackMethod = "fallbackCart")
    public ResponseEntity<Object> userAddCart(@RequestBody Cart cart) {
        int count = 1;
        System.out.println("Retry method called " + count++ + " times at " + new Date());
        HttpEntity<Cart> request = new HttpEntity<>(cart);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(
                BASE_URL + "api/cart/add",
                HttpMethod.POST,
                request,
                Object.class
        );
        return ResponseEntity.status(responseEntity.getStatusCode())
                .body(responseEntity.getBody());
    }


    @PostMapping("/user/order/add")
    @PreAuthorize("hasRole('USER')")
    @CircuitBreaker(name = UserService, fallbackMethod = "fallbackOrder")
    public ResponseEntity<Object> userAddOrder(@RequestBody @Valid Order order) {
        int count = 1;
        System.out.println("Retry method called " + count++ + " times at " + new Date());
        HttpEntity<Order> request = new HttpEntity<>(order);
        ResponseEntity<Object> responseEntity = restTemplate.exchange(
                BASE_URL + "api/order/add",
                HttpMethod.POST,
                request,
                Object.class
        );
        return ResponseEntity.status(responseEntity.getStatusCode())
                .body(responseEntity.getBody());
    }






}
