package com.iuh.serviceuser.entity;

public enum SizeClothes {
    S,
    M,
    L,
    XL,
    XXL,
    XXXL
}
