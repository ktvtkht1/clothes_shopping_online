package com.iuh.serviceuser.entity;

public enum ColorClothes {
    RED,
    BLUE,
    GREEN,
    YELLOW,
    BLACK,
    WHITE,
    PINK,
    PURPLE,
    ORANGE,
    BROWN,
    GREY
}
