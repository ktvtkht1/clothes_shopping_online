package com.iuh.serviceuser.entity;

public enum StatusOrder {
    PENDING,
    CONFIRMED,
    PROCESSING,
    SHIPPED,
    DELIVERED,
    CANCELLED,
    RETURNED

}
