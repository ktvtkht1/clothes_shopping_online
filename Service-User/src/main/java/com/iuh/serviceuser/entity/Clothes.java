package com.iuh.serviceuser.entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "clothes")
public class Clothes implements java.io.Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @NotBlank
    private String image;

    @NotNull
    private double price;

    @NotNull
    private int quantity;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SizeClothes size;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ColorClothes color;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CategoryClothes category;

    @NotBlank
    private String brand;
}
