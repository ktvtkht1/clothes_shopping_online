package com.iuh.inventoryservice.entity;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
@Entity
@Table(name = "t_inventory")
@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Inventory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String productId;
    private String description;
    private int quantity;

    public Inventory(String productId, String description, int quantity) {
        this.productId = productId;
        this.description = description;
        this.quantity = quantity;
    }
}
