package com.iuh.inventoryservice.controller;

import com.iuh.inventoryservice.dto.InventoryResponse;
import com.iuh.inventoryservice.entity.Inventory;

import com.iuh.inventoryservice.repository.InventoryRepository;

import com.iuh.inventoryservice.service.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/inventory")
public class InventoryController {

    private InventoryService inventoryService;

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = "application/json")
    public InventoryResponse saveNewInventory(@RequestBody InventoryResponse  inventoryResponse){
        return this.inventoryService.addNewInventory(inventoryResponse);
    }

    @GetMapping("/{id}")
    public InventoryResponse getInventory(@PathVariable Long id){
        return this.inventoryService.getInventory(id);
    }
    // Phương thức mới để lấy tất cả danh sách inventory
    @GetMapping("/all")
    public List<InventoryResponse> getAllInventory() {
        return this.inventoryService.getAllInventory();
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteInventory(@PathVariable Long id) {
        this.inventoryService.deleteInventory(id);
        System.out.println("Inventory with mã " + id + " has been deleted successfully.");
    }
    // Thêm phương thức PUT để sửa inventory
    @PutMapping("/{id}")
    public InventoryResponse updateInventory(@PathVariable Long id, @RequestBody InventoryResponse inventoryResponse) {
        return this.inventoryService.updateInventory(id, inventoryResponse);
    }

}
