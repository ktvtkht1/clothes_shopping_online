package com.iuh.inventoryservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InventoryResponse implements Serializable{
    private Long id;
    private String productId;
    private String description;
    private int quantity;
}
