package com.iuh.inventoryservice.service;

import com.iuh.inventoryservice.dto.InventoryResponse;
import com.iuh.inventoryservice.entity.Inventory;

import java.util.List;

public interface InventoryService {
    InventoryResponse addNewInventory(InventoryResponse inventoryResponse);
    InventoryResponse getInventory(Long id);
    List<InventoryResponse> getAllInventory();
    void deleteInventory(Long id); // Thêm phương thức delete
    InventoryResponse updateInventory(Long id, InventoryResponse inventoryResponse);
}
