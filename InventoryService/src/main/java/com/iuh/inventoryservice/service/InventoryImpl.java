package com.iuh.inventoryservice.service;

import com.iuh.inventoryservice.dto.InventoryResponse;
import com.iuh.inventoryservice.entity.Inventory;
import com.iuh.inventoryservice.repository.InventoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InventoryImpl implements InventoryService {
    private final InventoryRepository inventoryRepository;

    public InventoryImpl(InventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    @Override
    public InventoryResponse addNewInventory(InventoryResponse inventoryResponse) {
        Inventory inventory = new Inventory(
                inventoryResponse.getProductId(),
                inventoryResponse.getDescription(),
                inventoryResponse.getQuantity()
        );
        inventory = inventoryRepository.save(inventory);
        return new InventoryResponse(
                inventory.getId(),
                inventory.getProductId(),
                inventory.getDescription(),
                inventory.getQuantity()
        );
    }

    @Override
    public InventoryResponse getInventory(Long id) {
        return inventoryRepository.findById(id)
                .map(inventory -> new InventoryResponse(
                        inventory.getId(),
                        inventory.getProductId(),
                        inventory.getDescription(),
                        inventory.getQuantity()
                ))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Inventory not found"));
    }

    @Override
    public List<InventoryResponse> getAllInventory() {
        List<Inventory> inventories = inventoryRepository.findAll();
        return inventories.stream()
                .map(this::mapToInventoryResponse)
                .collect(Collectors.toList());
    }
    @Override
    public void deleteInventory(Long id) {
        if (inventoryRepository.existsById(id)) {
            inventoryRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Inventory not found");
        }
    }

    @Override
    public InventoryResponse updateInventory(Long id, InventoryResponse inventoryResponse) {
        Optional<Inventory> optionalInventory = inventoryRepository.findById(id);
        if (optionalInventory.isPresent()) {
            Inventory inventory = optionalInventory.get();
            inventory.setProductId(inventoryResponse.getProductId());
            inventory.setDescription(inventoryResponse.getDescription());
            inventory.setQuantity(inventoryResponse.getQuantity());
            inventory = inventoryRepository.save(inventory);
            return new InventoryResponse(
                    inventory.getId(),
                    inventory.getProductId(),
                    inventory.getDescription(),
                    inventory.getQuantity()
            );
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Inventory not found");
        }
    }

    private InventoryResponse mapToInventoryResponse(Inventory inventory) {
        return new InventoryResponse(
                inventory.getId(),
                inventory.getProductId(),
                inventory.getDescription(),
                inventory.getQuantity()
        );
    }
}
