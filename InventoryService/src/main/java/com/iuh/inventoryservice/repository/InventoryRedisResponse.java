//package com.iuh.inventoryservice.repository;
//
//import com.iuh.inventoryservice.entity.Inventory;
//import org.springframework.data.redis.core.HashOperations;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public class InventoryRedisResponse {
//    private HashOperations hashOperations;
//    private RedisTemplate redisTemplate;
//
//    public InventoryRedisResponse(RedisTemplate redisTemplate) {
//        this.hashOperations = redisTemplate.opsForHash();
//        this.redisTemplate = redisTemplate;
//    }
//    public void addNewInventory(Inventory inventory) {
//        hashOperations.put("INVENTORY", inventory.getId(), inventory);
//    }
//}
