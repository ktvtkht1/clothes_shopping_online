package com.example.clothesservice.service;

import com.example.clothesservice.entity.Clothes;
import com.example.clothesservice.exception.HandleException;
import com.example.clothesservice.exception.ServiceException;
import com.example.clothesservice.repository.ClothesRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class ClothesService {
    private final ClothesRepository clothesRepository;

    public ClothesService(ClothesRepository clothesRepository) {
        this.clothesRepository = clothesRepository;
    }

    public List<Clothes> getAllClothes() {
        return clothesRepository.findAll();
    }

    public Clothes addClothes(Clothes clothes) {
        return clothesRepository.save(clothes);
    }

    public Clothes updateClothes(Clothes clothes) {
        Clothes clothesUpdate = clothesRepository.findById(clothes.getId()).orElse(null);
        if (clothesUpdate == null) {
            log.error("Clothes not found");
            throw new ServiceException("Clothes not found with id : " + clothes.getId());
        }
        clothesUpdate.setName(clothes.getName());
        clothesUpdate.setDescription(clothes.getDescription());
        clothesUpdate.setImage(clothes.getImage());
        clothesUpdate.setPrice(clothes.getPrice());
        clothesUpdate.setQuantity(clothes.getQuantity());
        clothesUpdate.setSize(clothes.getSize());
        clothesUpdate.setColor(clothes.getColor());
        clothesUpdate.setCategory(clothes.getCategory());
        clothesUpdate.setBrand(clothes.getBrand());
        return clothesRepository.save(clothesUpdate);
    }

    public Clothes getClothes(Long id) {
        Clothes clothes = clothesRepository.findById(id).orElse(null);
        if (clothes == null) {
            log.error("Clothes not found");
            throw new ServiceException("Clothes not found with id : " + id);
        }
        return clothes;
    }

    public void deleteClothes(Long id) {
        Clothes clothes = clothesRepository.findById(id).orElse(null);
        if (clothes == null) {
            log.error("Clothes not found");
            throw new ServiceException("Clothes not found with id : " + id);
        }
        clothesRepository.delete(clothes);
    }
}
