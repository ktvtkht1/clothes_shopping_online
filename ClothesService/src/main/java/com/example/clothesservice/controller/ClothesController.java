package com.example.clothesservice.controller;

//import org.springframework.security.access.prepost.PreAuthorize;
import com.example.clothesservice.entity.Clothes;
import com.example.clothesservice.response.WrapResponse;
import com.example.clothesservice.service.ClothesService;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/clothes")
@Log4j2
public class ClothesController {
    @Autowired
    private ClothesService clothesService;



    @GetMapping("/all")
    public WrapResponse<List<Clothes>> getAllClothes() {
        log.info("Get all clothes");
        return WrapResponse.ok(clothesService.getAllClothes());
    }

    @PostMapping("/add")
    public WrapResponse<Clothes> addClothes(@RequestBody @Valid Clothes clothes) {
        log.info("Add clothes {} ", clothes);
        return WrapResponse.ok(clothesService.addClothes(clothes));
    }

    @PostMapping("/update")
    public WrapResponse<Clothes> updateClothes(@RequestBody @Valid Clothes clothes) {
        log.info("Update clothes {} ", clothes);
        return WrapResponse.ok(clothesService.updateClothes(clothes));
    }

    @GetMapping("/get/{id}")
    public WrapResponse<Clothes> getClothes(@PathVariable Long id) {
        log.info("Get clothes by id {} ", id);
        return WrapResponse.ok(clothesService.getClothes(id));
    }


    @PostMapping("/delete/{id}")
    public WrapResponse<String> deleteClothes(@PathVariable Long id) {
        log.info("Delete clothes by id {} ", id);
        clothesService.deleteClothes(id);
        return WrapResponse.ok("Delete clothes success");
    }

}
