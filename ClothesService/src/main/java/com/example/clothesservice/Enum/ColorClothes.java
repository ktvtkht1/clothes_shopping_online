package com.example.clothesservice.Enum;

public enum ColorClothes {
    RED,
    BLUE,
    GREEN,
    YELLOW,
    BLACK,
    WHITE,
    PINK,
    PURPLE,
    ORANGE,
    BROWN,
    GREY
}
