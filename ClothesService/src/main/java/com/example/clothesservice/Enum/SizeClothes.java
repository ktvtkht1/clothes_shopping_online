package com.example.clothesservice.Enum;

public enum SizeClothes {
    S,
    M,
    L,
    XL,
    XXL,
    XXXL
}
