package com.example.clothesservice.Enum;

public enum CategoryClothes {
    T_SHIRT,
    SHIRT,
    HOODIE,
    JACKET,
    JEANS,
    SHORTS,
    PANTS,
    DRESS,
    SKIRT,
    SHOES,
    HAT,
    BAG,
    WATCH,
    GLASSES,
    BELT,
    SCARF,
    GLOVES,
    SOCKS,
    UNDERWEAR,
    SWIMWEAR,
    Traditional_Wear
}
