package com.example.clothesservice.repository;

import com.example.clothesservice.entity.Clothes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClothesRepository extends JpaRepository<Clothes, Long>{
    List<Clothes> findAll();
}
