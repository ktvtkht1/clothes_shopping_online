package com.example.cartservice.service;


import com.example.cartservice.entity.Cart;
import com.example.cartservice.entity.CartItem;
import com.example.cartservice.exception.ServiceException;
import com.example.cartservice.repository.CartRepository;
import jakarta.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Log4j2
public class CartService {
    private final CartRepository cartRepository;


    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }


    @Transactional // Ensure this method runs within a transactional context
    public List<Cart> getAllCart() {
        // Fetch Carts along with their Cart items eagerly
        List<Cart> carts = cartRepository.findAllWithCartItems();
        return carts;
    }

    public Cart addCart(Cart cart) {
        Long totalPrice = calculateTotalPrice(cart.getCartItems());
        Long totalQuantity = calculateTotalQuantity(cart.getCartItems());
        cart.setTotalPrice(totalPrice);
        cart.setTotalQuantity(totalQuantity);
        cart.setCreatedAt(LocalDateTime.now());

        // Lưu đối tượng order vào cơ sở dữ liệu và trả về
        return cartRepository.save(cart);
    }

    private Long calculateTotalPrice(List<CartItem> cartItems) {
        Long totalPrice = 0L;
        for (CartItem cartItem : cartItems) {
            totalPrice += cartItem.getPrice() * cartItem.getQuantity();
        }
        return totalPrice;
    }

    private Long calculateTotalQuantity(List<CartItem> cartItems) {
        Long totalQuantity = 0L;
        for (CartItem cartItem : cartItems) {
            totalQuantity +=  cartItem.getQuantity();
        }
        return totalQuantity;
    }





    public Cart updateCart(Cart cart) {
        // Check if the Cart object has a non-null ID
        if (cart.getId() == null) {
            log.error("Cart ID is null");
            throw new ServiceException("Cart ID is null");
        }

        // Retrieve the existing order from the database
        Cart existingCart = cartRepository.findById(cart.getId())
                .orElseThrow(() -> {
                    log.error("Cart not found with ID: {}", cart.getId());
                    return new ServiceException("Cart not found with ID: " + cart.getId());
                });

        // Update the existing order with the new values
        existingCart.setUserId(cart.getUserId());
        existingCart.setCartItems(cart.getCartItems());
        existingCart.setTotalPrice(calculateTotalPrice(cart.getCartItems()));
        existingCart.setTotalQuantity(calculateTotalQuantity(cart.getCartItems()));
        existingCart.setUpdatedAt(LocalDateTime.now());



        // Save and return the updated order
        return cartRepository.save(existingCart);
    }


    public Cart getCart(Long id) {
        Cart cart = cartRepository.findById(id).orElse(null);
        if (cart == null) {
            log.error("cart not found");
            throw new ServiceException("cart not found with id : " + id);
        }
        return cart;
    }

    public void deleteCart(Long id) {
        Cart cart = cartRepository.findById(id).orElse(null);
        if (cart == null) {
            log.error("Cart not found");
            throw new ServiceException("Cart not found with id : " + id);
        }
        cartRepository.delete(cart);
    }
}
