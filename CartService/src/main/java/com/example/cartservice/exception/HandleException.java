package com.example.cartservice.exception;



import com.example.cartservice.constant.WrapResponseStatus;
import com.example.cartservice.response.WrapResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
@RestControllerAdvice
public class HandleException extends ResponseEntityExceptionHandler {


    @ExceptionHandler(ServiceException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public WrapResponse handleServiceException(ServiceException ex) {
        System.err.println("!!!==>Handle Error: "+ex.getMessage());
        return WrapResponse.error(WrapResponseStatus.SERVICE_UNAVAILABLE,ex.getMessage());
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public WrapResponse resourceException(ServiceException ex) {
        System.err.println("!!!==>Handle Error: "+ex.getMessage());
        return WrapResponse.error(WrapResponseStatus.INTERNAL_SERVER_ERROR,ex.getMessage()== null ? "Internal Server Error" : ex.getMessage());
    }


}
