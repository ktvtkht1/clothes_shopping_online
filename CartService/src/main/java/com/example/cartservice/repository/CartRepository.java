package com.example.cartservice.repository;


import com.example.cartservice.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long>{
    @Query("SELECT DISTINCT o FROM Cart o LEFT JOIN FETCH o.cartItems")
    List<Cart> findAllWithCartItems();


}
