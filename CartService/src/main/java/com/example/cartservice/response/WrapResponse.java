package com.example.cartservice.response;


import com.example.cartservice.constant.WrapResponseStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WrapResponse<T> {
    private String statusCode;
    private T data;
    private String message;

    public static <T> WrapResponse<T> ok(T data) {
        WrapResponse<T> response = new WrapResponse();
        response.setStatusCode(WrapResponseStatus.SUCCESS);
        response.setData(data);

        return response;
    }

    public static <T> WrapResponse<T> error(String message) {
        WrapResponse<T> response = new WrapResponse();
        response.setData(null);
        response.setStatusCode(WrapResponseStatus.INTERNAL_SERVER_ERROR);
        response.setMessage(message);


        return response;
    }

    public static <T>WrapResponse<T> error (String statusCode, String message){
        WrapResponse<T> wrapRespone = new WrapResponse<>();
        wrapRespone.setStatusCode(statusCode);
        wrapRespone.setMessage(message);
        wrapRespone.setData(null);
        return wrapRespone;
    }
}
