package com.example.cartservice.controller;

//import org.springframework.security.access.prepost.PreAuthorize;

import com.example.cartservice.entity.Cart;
import com.example.cartservice.response.WrapResponse;
import com.example.cartservice.service.CartService;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cart")
@Log4j2
public class CartController {
    @Autowired
    private CartService cartService;
//
//
//
    @GetMapping("/all")
    public WrapResponse<List<Cart>> getAllOrder() {
        log.info("Get all Cart");
        return WrapResponse.ok(cartService.getAllCart());
    }
//
    @PostMapping("/add")
    public WrapResponse<Cart> addCart(@RequestBody @Valid Cart cart) {
        log.info("Add cart {} ", cart);
        return WrapResponse.ok(cartService.addCart(cart));
    }

    @PostMapping("/update")
    public WrapResponse<Cart> updateCart(@RequestBody @Valid Cart cart) {
        log.info("Update Cart {} ", cart);
        return WrapResponse.ok(cartService.updateCart(cart));
    }

    @GetMapping("/get/{id}")
    public WrapResponse<Cart> getCart(@PathVariable Long id) {
        log.info("Get Cart by id {} ", id);
        return WrapResponse.ok(cartService.getCart(id));
    }


    @PostMapping("/delete/{id}")
    public WrapResponse<String> deleteCart(@PathVariable Long id) {
        log.info("Delete Cart by id {} ", id);
        cartService.deleteCart(id);
        return WrapResponse.ok("Delete Cart success");
    }

}
